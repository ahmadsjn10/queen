(function ($) {
    const fileInfo = document.querySelector('.file-info');
    const realInput = document.getElementById('real-input');
    
    $('.browse-btn').click(function(event){
        event.preventDefault();
        realInput.click();
    })

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
        $('#imgPreview').removeClass("d-none");
    }
    
    $('#real-input').change(function() {
        readURL(this);
        var filename = $(realInput).val();
        $(fileInfo).html(filename);
    });
})(jQuery);