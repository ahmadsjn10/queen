(function ($) {
    $('.select-category').selectize({
        sortField: 'text',
    });
    
    var selectKeyword = $('.select-keywords').selectize({
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        plugins: ['remove_button'],
        options: [],
        items: [],
        maxItems: 10,
        create: function(input, callback) {
            return callback({
                id: input,
                name : input
            });
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/json-keywords',
                type: 'GET',
                dataType: 'json',
                data: {
                    name: query,
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res);
                }
            })
        }
    })
    
    if(selectKeyword.length > 0){
        const el = $('.select-keywords');
        const attr = el.attr('data-selected');
        var elm = selectKeyword[0].selectize;
        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if (typeof attr !== typeof undefined && attr !== false) {
            if(el.data('selected') && el.data('selected').length > 0){
                var arrayData = el.data('selected');
                $(arrayData).each(function(idx, data){
                    if(data.name){
                        var newData = {'id': idx+1, 'name':data.name};
                    }else{
                        var newData = {'id': idx+1, 'name':data};
                    }
                    elm.addOption(newData);
                    elm.refreshOptions(); 
                    elm.addItem(newData.name); 
                })
            }
        }
    }
})(jQuery);
