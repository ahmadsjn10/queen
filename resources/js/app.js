
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('trumbowyg/dist/trumbowyg.min');
require('parsleyjs');
import toastr from 'toastr';
import selectize from 'selectize';
import SomeIcon from '../../node_modules/trumbowyg/dist/ui/icons.svg';
require('./dist/selectize_custome');
require('./dist/sidebar');
require('./dist/upload_file');

(function ($) {
    $(function () {
        $('#description').trumbowyg({
            svgPath: SomeIcon
        });

        if($(".errorValidateToastr").length > 0){
            $(".errorValidateToastr").each(function(index, data){
                // console.log(data);
                // console.log($(data).find(".msg").text());
                // toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.options.timeOut = 0;
                toastr.options.extendedTimeOut = 0;
                toastr.error($(data).find(".msg").text());
                // toastr.error('aaaaaa');
                // switch($(data).find(".status").text()) {
                //     case "success":
                //         toastr.options.timeOut = 5000;
                //         toastr.success($("#toastrMessage .msg").text());
                //     break;
                //     case "errors":
                //     break;
                //     case "persistents":
                //         toastr.warning($("#toastrMessage .msg").text());
                //     break;
                // }
            })
        }
        
        if($('#form-parsley').length > 0) {
            $('#form-parsley').parsley({
                trigger:      'change',
                errorClass: "is-invalid",
                errorsWrapper: '<div class="invalid-feedback"></div>',
                errorTemplate: '<strong></strong>',
            });
        }
    })
}) ( jQuery );