@extends('layouts.dashboard')

@section('content')
<section class="content-header m-t-5">
  <h3>
      {{__("Detail Article")}}
  </h3>
  @include('admin.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group row">
                <label for="title" class="col-md-2">{{ __('Title') }}</label>
                <div class="col-md-8">
                    <div>{{ $article->title }}</div>
                </div>
            </div>
            <div class="form-group row">
                <label for="category" class="col-md-2">{{ __('Category') }}</label>
                <div class="col-md-8">
                    <div>{{ $article->category->name }}</div>
                </div>
            </div>
            <div class="form-group row">
                <label for="keyword" class="col-md-2">{{ __('Keywords') }}</label>
                <div class="col-md-8">
                    @foreach ($article->keywordArticles as $key=>$item)
                    {{ $item->keyword->name }}@if($key+1 < $article->keywordArticles->count()),@endif
                    @endforeach
                </div>
            </div>
            <div class="form-group row">
                <label for="cover" class="col-md-2">{{ __('Cover') }}</label>
                <div class="col-md-8">
                        <img class="col-md-12 p-initial" src="{{asset('storage/covers/'.$article->cover)}}" alt="cover">
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-md-2">{{ __('Description') }}</label>
                <div class="col-md-8">
                    {!! $article->description !!} 
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-md-2"></label>
                <div class="col-md-8">
                    <a href="{{ route('articles.index') }}" class="btn btn-primary" role="button">{{ __("Back") }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection