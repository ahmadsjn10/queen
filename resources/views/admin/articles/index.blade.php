@extends('layouts.dashboard')

@section('content')
	<section class="content-header m-t-5">
        <h3>
            {{__("List Articles")}}
        </h3>
        @include('admin.breadcrumb')
	</section>
	<section class="content">
	    <div class="row">
	        <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <form method="GET" action="#">
                            <div class='form-group clearfix'>
                                <div class='col-md-4'>
                                    <div class="input-group custom-search-form">
                                        <input type="text" class="form-control" name="search" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> {{__("Search")}}</button>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2">
                        <div class='float-right'>
                            <a href="{{ route('articles.create') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i> {{__("Add")}}</a>
                        </div>
                    </div>
                </div>
                <div class="scroll-x-auto">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>{{__("No.")}}</th>
                            <th>{{__("Title")}}</th>
                            <th>{{__("Category")}}</th>
                            <th>{{__("Keywords")}}</th>
                            <th class='text-center'>{{__("Actions")}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($articles as $i=>$article)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td> {{ $article->title }} </td>
                                <td> {{ $article->category->name }} </td>
                                <td>
                                    @foreach ($article->keywordArticles as $key=>$item)
                                        {{ $item->keyword->name }}@if($key+1 < $article->keywordArticles->count()),@endif
                                    @endforeach
                                </td>
                                <td class='text-center'>
                                    <div class="btn-group">
                                        <a class="m-r-10" href='{{ route('articles.edit', [$article]) }}'><span class='fas fa-edit'></span></a>		
                                        <a href='{{  route('articles.show', [$article]) }}'><span class="fas fa-eye"></span></a>
                                        <form id="delete_article{{$article->id}}" action='{{  route('articles.destroy', [$article]) }}' method="POST">
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <a class="m-l-10" href="#" onclick="document.getElementById('delete_article{{$article->id}}').submit();"><span class='fas fa-trash' ></span></a>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $articles->onEachSide(1)->links() }}
			</div>
		</div>
	</section>
@endsection