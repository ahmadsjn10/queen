@extends('layouts.dashboard')

@section('content')
<section class="content-header m-t-5">
    <h3>
        @if(!empty($article->getKey()))
            {{ __("Update Article") }}
        @else
            {{ __("Create New Article") }}
        @endif
    </h3>
    @include('admin.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ !empty($article->getKey()) ? route('articles.update', [$article]) : route('articles.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @if(!empty($article->getKey()))
                    {{ method_field('PATCH') }}
                @endif

                <div class="form-group row">
                    <label for="title" class="col-md-2">{{ __('Title') }}</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="title" id="title" value="{{ old('title') ?? $article->title ?? '' }}" placeholder="{{__("Input a title...")}}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="category" class="col-md-2">{{ __('Category') }}</label>
                    <div class="col-md-8">
                        <select id="category" class="select-category" placeholder="{{__("Select a category...")}}" name="category"> 
                            <option value=""></option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}" {{ (old('category') ?? $article->category_id ?? '') == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="cover" class="col-md-2">{{ __('Cover') }}</label>
                    <div class="col-md-8">
                        <div class="input-container">
                            <input type="file" id="real-input" accept="image/jpeg, image/png" class="form-control" name="cover" id="cover" value=""  >
                            <button class="browse-btn">
                                {{__("Browse file")}}
                            </button>
                            <span class="file-info">{{__("Upload a file...")}}</span>
                        </div>
                        <div id="imgPreview" @if(empty($article->cover)) class="d-none" @endif>
                            <img class="col-md-12 img-preview p-initial m-t-10" src="{{ $article->cover ? asset('/storage/covers/'.$article->cover) : '' }}" alt="cover">
                        </div>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="keyword" class="col-md-2">{{ __('Keyword') }}</label>
                    <div class="col-md-8">
                    <select @if(old('keyword')) data-selected="{{ json_encode(old('keyword')) }}" @elseif(!empty($article->getKey())) data-selected="{{ json_encode($keywordArticles) }}" @endif class="select-keywords" id="keyword" placeholder="{{__("Pick a keyword...")}}" name="keyword[]" value="" multiple></select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-md-2">{{ __('Description') }}</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="description" id="description">
                            {!! old('description') ?? $article->description ?? '' !!}
                        </textarea>
                    </div>
                </div>

                <div class="form-group row m-b-10">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="button-group button-group-form">
                            <a href="{{ action('ArticleController@index') }}" class="btn btn-md btn-secondary">{{ __('Cancel') }}</a>
                            @if(!empty($article->getKey()))
                                <button type="submit" class="btn btn-md btn-primary">
                                    {{ __("Update")  }}
                                </button>
                            @else
                                <button type="submit" class="btn btn-md btn-primary">
                                    {{ __("Create")  }}
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
