@extends('layouts.dashboard')

@section('content')
<section class="content-header m-t-5">
    <h3>
        @if(!empty($category->getKey()))
            {{ __("Update Category") }}
        @else
            {{ __("Create New Category") }}
        @endif
    </h3>
    @include('admin.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ !empty($category->getKey()) ? route('categories.update', [$category]) : route('categories.store') }}" method="post">
                @csrf
                @if(!empty($category->getKey()))
                    {{ method_field('PATCH') }}
                @endif

                <div class="form-group row">
                    <label for="name" class="col-md-2">{{ __('Name') }}</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name" id="name" value="{{ old('name') ?? $category->name ?? '' }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="button-group button-group-form">
                            <a href="{{ action('CategoryController@index') }}" class="btn btn-md btn-secondary">{{ __('Cancel') }}</a>
                            @if(!empty($category->getKey()))
                                <button type="submit" class="btn btn-md btn-primary">
                                    {{ __("Update")  }}
                                </button>
                            @else
                                <button type="submit" class="btn btn-md btn-primary">
                                    {{ __("Create")  }}
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
