@extends('layouts.dashboard')

@section('content')
<section class="content-header m-t-5">
  <h3>
      {{__("Detail Keyword")}}
  </h3>
  @include('admin.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group row">
                <label for="name" class="col-md-2">{{ __('Name') }}</label>
                <div class="col-md-8">
                    {{ $keyword->name }} 
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-md-2"></label>
                <div class="col-md-8">
                    <a href="{{ route('keywords.index') }}" class="btn btn-primary" role="button">{{ __("Back") }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection