@extends('layouts.dashboard')

@section('content')
<section class="content-header m-t-5">
    <h3>
        @if(!empty($keyword->getKey()))
            {{ __("Update Keyword") }}
        @else
            {{ __("Create New Keyword") }}
        @endif
    </h3>
    @include('admin.breadcrumb')
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ !empty($keyword->getKey()) ? route('keywords.update', [$keyword]) : route('keywords.store') }}" method="post">
                @csrf
                @if(!empty($keyword->getKey()))
                    {{ method_field('PATCH') }}
                @endif

                <div class="form-group row">
                    <label for="name" class="col-md-2">{{ __('Name') }}</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name" id="name" value="{{ old('name') ?? $keyword->name ?? '' }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="button-group button-group-form">
                            <a href="{{ action('KeywordController@index') }}" class="btn btn-md btn-secondary">{{ __('Cancel') }}</a>
                            @if(!empty($keyword->getKey()))
                                <button type="submit" class="btn btn-md btn-primary">
                                    {{ __("Update")  }}
                                </button>
                            @else
                                <button type="submit" class="btn btn-md btn-primary">
                                    {{ __("Create")  }}
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
