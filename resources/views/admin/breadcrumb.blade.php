<div class="row">
    <div class="col-12 breadcrumb-wrapper">
        <!-- breadcrumbs -->
        @php $url = ""; $counter = 0; $fullURL = Request::segments();@endphp
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">{{ __('Dashboard') }}</a>
            </li>
            @foreach($fullURL as $nav)
                @php $counter++; @endphp
                @php $url .= "/" . $nav; @endphp
                <li class="breadcrumb-item">
                    <a href="{{ $url }}">{{ __(ucwords(str_replace(['-', '_'], ' ', $nav))) }}</a>
                </li>
            @endforeach
        </ol>
        <!-- breadcrumbs -->
    </div>
</div>