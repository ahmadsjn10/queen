<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Queen')}}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{ asset('images/favicon.ico')}}" type="image/x-icon">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        window.appUrl = '{!! json_encode(env('APP_URL')) !!}';
    </script>
</head>
<body>
    <!-- NavBar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-primary">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <a class="navbar-brand" href="#">
            <span class="fas fa-chess-queen" width="30" height="30" class="d-inline-block align-top"></span>
            <span class="menu-collapsed">My Bar</span>
        </a>
        <a href="#" data-toggle="sidebar-colapse" class="align-items-center show-large" id="btn-collapse">
            <div class="d-flex w-100 align-items-center">
                <span id="collapse-icon" class="fa fa-2x mr-3"></span>
            </div>
        </a>
        <div class="navbar-collapse collapse" id="collapsingNavbar">
            <ul class="navbar-nav">
                
            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- Navbar -->
                {{-- <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-bell fa-fw"></i>
                        <span class="badge badge-danger">9+</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-envelope fa-fw"></i>
                        <span class="badge badge-danger">7</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li> --}}
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-user-circle fa-fw"></i> {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        {{-- <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div> --}}
                        <a class="dropdown-item" 
                            href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                <li class="nav-item dropdown d-sm-block d-md-none">
                    <a class="nav-link dropdown-toggle" href="#" id="smallerscreenmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ __("Menu") }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="smallerscreenmenu">
                        <a class="dropdown-item" href="{{ route('categories.index') }}">{{ __("Categories") }}</a>
                        <a class="dropdown-item" href="{{ route('keywords.index') }}">{{ __("Keywords") }}</a>
                        <a class="dropdown-item" href="{{ route('articles.index') }}">{{ __("Articles") }}</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- NavBar END -->

    <!-- Bootstrap row -->
    <div class="row" id="body-row">
        <!-- Sidebar -->
        <div id="sidebar-container" class="sidebar-expanded d-none d-md-block"><!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
            <!-- Bootstrap List Group -->
            <ul class="list-group">
                <!-- Separator with title -->
                <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                    <small>MAIN MENU</small>
                </li>
                <!-- /END Separator -->
                <!-- Menu with submenu -->
                <a aria-expanded="false" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-start align-items-center">
                        <span class="fas fa-tachometer-alt fa-fw mr-3"></span> 
                        <span class="menu-collapsed">{{ __("Dashboard") }}</span>
                    </div>
                </a>
                <a href="#submenu2" data-toggle="collapse" aria-expanded="false" class="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-start align-items-center">
                        <span class="fas fa-bars fa-fw mr-3"></span>
                        <span class="menu-collapsed">{{ __("Menu") }}</span>
                        <span class="menu-collapsed fas fa-chevron-down ml-auto"></span>
                    </div>
                </a>
                <!-- Submenu content -->
                <div id='submenu2' class="collapse sidebar-submenu">
                    <a href="{{ route('categories.index') }}" class="list-group-item list-group-item-action bg-dark text-white">
                        <span class="menu-collapsed">{{__("Categories")}}</span>
                    </a>
                    <a href="{{ route('keywords.index') }}" class="list-group-item list-group-item-action bg-dark text-white">
                        <span class="menu-collapsed">{{__("Keywords")}}</span>
                    </a>
                    <a href="{{ route('articles.index') }}" class="list-group-item list-group-item-action bg-dark text-white">
                            <span class="menu-collapsed">{{__("Articles")}}</span>
                        </a>
                </div>            
                <!-- Separator with title -->
                {{-- <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                    <small>OPTIONS</small>
                </li> --}}
                <!-- /END Separator -->
            </ul><!-- List Group END-->
        </div>
        <!-- sidebar-container END -->
    
        <!-- MAIN -->
        <div class="col">
            <!-- Page content -->
            <div class="container">
                <div class="d-none" id="toastrMessage">
                    @foreach($errors->all() as $toastr)
                        <div class="errorValidateToastr">
                            <div class="d-none msg">{{ $toastr }}</div>
                        </div>
                    @endforeach
                </div>
                

                <!-- Keep all page content within the page-content inset div! -->
                @yield('content')
            </div>
            <!-- Page content END -->

            <!-- Footer -->
            <footer class="sticky-footer">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                    <span>Copyright © <?= "Queen ". Date('Y') ?></span>
                    </div>
                </div>
            </footer>
            <!-- Footer END -->
        </div>
        <!-- Main Col END -->
    </div>
    <!-- body-row END -->
</body>
</html>
