<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

// auth
Auth::routes();

// categories
Route::resource('/categories', 'CategoryController');
// keywords
Route::resource('/keywords', 'KeywordController');
Route::get('/json-keywords', 'KeywordController@getKeywords')->name('keywords.json');
// articles
Route::resource('/articles', 'ArticleController');