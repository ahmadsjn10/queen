<?php
use Illuminate\Support\Facades\Storage;

if (!function_exists('storage')) {
    /**
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    function storage()
    {
        return Storage::disk(config('filesystems.default'));
    }
}