<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::whereNull('deleted_at')->paginate();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
        return view('admin.categories.form', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $this->validator()->validate();
        DB::beginTransaction();
        try {
            $category->create($request->only(['name']));
            
            DB::commit();
            
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Category has been created'),
                    'data' => $category
                ]);
            }
            return redirect()->route('categories.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.form', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validator()->validate();
        DB::beginTransaction();
        try {
            $category->update($request->only(['name']));
            
            DB::commit();
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Category has been updated'),
                    'data' => $category
                ]);
            }
            return redirect()->route('categories.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        DB::beginTransaction();
        try {
            $category->deleted_at = Carbon::now();
            $category->save();

            DB::commit();
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Category has been deleted'),
                    'data' => $category
                ]);
            }
            return redirect()->route('categories.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Get create and update validator
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator()
    {
        $payload = request()->only([
            'name',
        ]);

        return Validator::make($payload, [
            'name'=> 'required|max:100|unique:categories',
        ]);
    }
}
