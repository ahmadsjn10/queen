<?php

namespace App\Http\Controllers;

use App\Models\Keyword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class KeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keywords = Keyword::whereNull('deleted_at')->paginate();
        return view('admin.keywords.index', compact('keywords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $keyword = new Keyword();
        return view('admin.keywords.form', compact('keyword'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Keyword $keyword)
    {
        $this->validator()->validate();
        DB::beginTransaction();
        try {
            $data['name'] = trim(ucfirst($request->name));
            $keyword->create($data);
            DB::commit();
            
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Keyword has been created'),
                    'data' => $keyword
                ]);
            }
            
            return redirect()->route('keywords.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function show(Keyword $keyword)
    {
        return view('admin.keywords.show', compact('keyword'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function edit(Keyword $keyword)
    {
        return view('admin.keywords.form', compact('keyword'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keyword $keyword)
    {
        $this->validator()->validate();
        DB::beginTransaction();
        try {
            $data['name'] = trim(ucfirst($request->name));
            $keyword->update($data);
            
            DB::commit();
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Keyword has been updated'),
                    'data' => $keyword
                ]);
            }
            return redirect()->route('keywords.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyword $keyword)
    {
        DB::beginTransaction();
        try {
            $keyword->deleted_at = Carbon::now();
            $keyword->save();
            DB::commit();

            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Keyword has been deleted'),
                    'data' => $keyword
                ]);
            }
            return redirect()->route('keywords.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Get create and update validator
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator()
    {
        $payload = request()->only([
            'name',
        ]);

        return Validator::make($payload, [
            'name'=> 'required|max:100|unique:keywords',
        ]);
    }

    /**
     * Get data json
     * @return \Illuminate\Http\Response
     */
    public function getKeywords(){
        $keywords = Keyword::select('id','name')->whereNull('deleted_at')->get();
        return response()->json($keywords);
    }
}
