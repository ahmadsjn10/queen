<?php

namespace App\Http\Controllers;

use App\Models\ImageRelated;
use Illuminate\Http\Request;

class ImageRelatedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ImageRelated  $imageRelated
     * @return \Illuminate\Http\Response
     */
    public function show(ImageRelated $imageRelated)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ImageRelated  $imageRelated
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageRelated $imageRelated)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ImageRelated  $imageRelated
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageRelated $imageRelated)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ImageRelated  $imageRelated
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageRelated $imageRelated)
    {
        //
    }
}
