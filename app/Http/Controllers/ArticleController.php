<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Keyword;
use App\Models\KeywordArticle;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('created_at','desc')->paginate();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new Article();
        $categories = Category::select('id','name')->whereNull('deleted_at')->get();
        return view('admin.articles.form', compact('article','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Article $article)
    {
        $this->validator($article)->validate();
        
        DB::beginTransaction();
        try {
            $dataRequest = $request->only(['title', 'description']);
            if ($request->hasFile('cover')) {
                $file = $request->file('cover');
                $extension = $file->getClientOriginalExtension();
                $dir = 'covers';
                $fileName = Str::random(15) . '.' . $extension;

                $request->cover->storeAs($dir, $fileName, 'public');
                $dataRequest['cover'] = $fileName;
            }
            $dataRequest['category_id'] = $request->category;
            $dataRequest['user_id'] = auth()->id();
            $dataRequest['updated_by'] = auth()->id();
            $newArticle = $article->create($dataRequest);
            
            foreach ($request->keyword as $value) {
                $findKeyword = Keyword::whereRaw('LOWER(name) = ? ',[trim(strtolower($value))])->first();
                if(!empty($findKeyword)){
                    KeywordArticle::updateOrCreate(['keyword_id'=>$findKeyword->id, 'article_id'=>$newArticle->id]);
                }else{
                    $data['name'] = ucfirst($value);
                    $keyword = Keyword::create($data);
                    KeywordArticle::updateOrCreate(['keyword_id'=>$keyword->id, 'article_id'=>$newArticle->id]);
                }
            }

            DB::commit();
            
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Article has been created'),
                    'data' => $article
                ]);
            }
            return redirect()->route('articles.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('admin.articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $keywordArticles = KeywordArticle::join('keywords','keywords.id','=','keyword_articles.keyword_id')
        ->select('keywords.id','keywords.name')
        ->where('keyword_articles.article_id', $article->id)
        ->get()->toArray();

        $categories = Category::select('id','name')->whereNull('deleted_at')->get();
        return view('admin.articles.form', compact('article','categories','keywordArticles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $this->validator($article)->validate();
        
        DB::beginTransaction();
        try {
            $dataRequest = $request->only(['title', 'description']);
            if ($request->hasFile('cover')) {
                $file = $request->file('cover');
                $extension = $file->getClientOriginalExtension();
                $dir = 'covers';
                $fileName = Str::random(15) . '.' . $extension;
                storage()->delete($dir . '/' . $article->cover);
                $request->cover->storeAs($dir, $fileName, 'public');
                $dataRequest['cover'] = $fileName;
            }
            $dataRequest['category_id'] = $request->category;
            $dataRequest['updated_by'] = auth()->id();
            $article->update($dataRequest);
            
            if(!empty($request->keyword)){
                KeywordArticle::where('article_id', $article->id)->delete();
                foreach ($request->keyword as $value) {
                    $findKeyword = Keyword::whereRaw('LOWER(name) = ? ',[trim(strtolower($value))])->first();
                    if(!empty($findKeyword)){
                        KeywordArticle::updateOrCreate(['keyword_id'=>$findKeyword->id, 'article_id'=>$article->id]);
                    }else{
                        $data['name'] = ucfirst($value);
                        $keyword = Keyword::create($data);
                        KeywordArticle::updateOrCreate(['keyword_id'=>$keyword->id, 'article_id'=>$article->id]);
                    }
                }
            }

            DB::commit();
            
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Article has been created'),
                    'data' => $article
                ]);
            }
            return redirect()->route('articles.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        DB::beginTransaction();
        try {
            $article->delete();

            DB::commit();
            if (request()->wantsJson()) {
                return response()->json([
                    'message' => __('Article has been deleted'),
                    'data' => $article
                ]);
            }
            return redirect()->route('articles.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * Get create and update validator
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator($article)
    {
        $payload = request()->only([
            'title',
            'description',
            'cover',
            'category',
            'keyword',
        ]);

        $data = [
            'title'=> 'required|max:255',
            'description'=> 'required',
            'category'=> 'required|exists:categories,id',
            'keyword'=> 'required|present|array',
        ];

        if(!empty($article->getKey())){
            $data['cover'] = 'image';
        }else{
            $data['cover'] = 'required|image';
        }

        return Validator::make($payload, $data);
    }

    public function getKeywords(Request $request){
        $keywords = Keyword::select('id','name')
        ->whereNull('deleted_at')
        ->where('name', 'like', '%'.$request->get('name').'%')
        ->get();

        return response()->json($keywords);
    }
}
