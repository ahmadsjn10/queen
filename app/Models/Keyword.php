<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
    
    /**
     * Get related articles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keywordArticles()
    {
        return $this->hasMany(KeywordArticle::class);
    }
}
