<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeywordArticle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'keyword_id'
    ];

    /**
    * Get related account
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id');
    }

    /**
    * Get related account
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function keyword()
    {
        return $this->belongsTo(Keyword::class, 'keyword_id');
    }
}
