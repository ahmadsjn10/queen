<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'category_id',
        'user_id',
        'updated_by',
        'cover'
    ];

    /**
    * Get related account
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * Get related articles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keywordArticles()
    {
        return $this->hasMany(KeywordArticle::class);
    }
}
